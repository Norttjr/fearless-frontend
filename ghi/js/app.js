window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    function createCard(title, description, pictureUrl, startDates, endDates, locationName) {
        return `
        <div class="shadow p-3 mb-5 bg-body rounded">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <p class="card-text">${description}</p>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <div class="card-footer">
                ${startDates} - ${endDates}    
                </div>
                </div>
            </div>
        </div>
        `;
      }
  
      try {
        const response = await fetch(url);
    
        if (!response.ok) {
          // Figure out what to do when the response is bad
          const aHHHelp = `<div class="alert alert-warning" role="alert">
          A simple warning alert with <a href="#" class="alert-link">
            Fetch Didn't Happen, Gretchen
        </div>` 
              const column = document.querySelector('.col');
              column.innerHTML += aHHHelp;


        } else {
          const data = await response.json();
          let counter = 0;
          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const startDates = new Date(details.conference.starts).toDateString();
              const endDates = new Date(details.conference.starts).toDateString();
              const pictureUrl = details.conference.location.picture_url;
              const locationName = details.conference.location.name;
              const html = createCard(title, description, pictureUrl, startDates, endDates, locationName);
              const column = document.querySelector('#col' + counter % 3 );
              column.innerHTML += html;
              counter += 1;
              
            }
          }
    
        }
      } catch (e) {
        console.error("Error", e);
        // Figure out what to do if an error is raised
      }
    
    });
